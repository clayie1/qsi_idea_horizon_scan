# qsi_idea_horizon_scan

sandbox repo for scanning, diffing and compiling horizon exports

## input data

obtained from: https://share.nibr.novartis.net/communities/portfoliosystems/_layouts/15/start.aspx#/Horizon Reports/Forms/AllItems.aspx?RootFolder=%2fcommunities%2fportfoliosystems%2fHorizon%20Reports%2fNIBR%20Clinical%20Portfolio%20Extract&FolderCTID=0x012000C2F69E26682DD0409FB5CBF66B5EF2EF
access provided by: Matt Riddell & Neave Connolly

## output
- locate updated projects since last upload
- locate projects within reasonable timeframes prior to FPFV, sPOC
- track transitions (eg LPLV, CSR, other declarations
- aggregate by TA